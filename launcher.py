#!/usr/bin/env python
# coding: utf-8

from collections import Counter
from geopy.distance import great_circle
import csv


class Node:
    def __init__(self, cluster=-1, x=0.0, y=0.0, label="", data=""):
        self.cluster = cluster
        self.x = x
        self.y = y
        self.label = label
        self.data = data


def expandCluster(points, point, cluster, eps, minPts, regionQuery):
    seeds = regionQuery(points, point, eps)
    if len(seeds) < minPts:
        point.cluster = 0
        return False
    else:
        for s in seeds:
            s.cluster = cluster
        seeds.remove(point)
        while len(seeds) > 0:
            currentPoint = seeds[0]
            result = regionQuery(points, currentPoint, eps)
            if len(result) >= minPts:
                for r in result:
                    if r.cluster <= 0:
                        if r.cluster == -1:
                            seeds.append(r)
                        r.cluster = cluster
            seeds.remove(currentPoint)
        return True


def dbscan(points, eps, minPts, regionQuery):
    clusterId = 1 #NOISE = 0
    for point in points:
        if point.cluster == -1:
            if expandCluster(points, point, clusterId, eps, minPts, regionQuery):
                clusterId += 1

if __name__ == '__main__':
    '''
    def regionQuery(points, point, eps):
        return [row for row in points if sqrt(pow(point.x - row.x, 2) + pow(point.y - row.y,2)) <= eps]
    '''
    def region_query(points, point, eps):
        return [row for row in points if great_circle((point.x, point.y), (row.x, row.y)).meters <= eps]

    nodes = []
    with open('vivreRennes.csv', 'r') as f: # From https://data.rennesmetropole.fr/explore/dataset/base-orga-var/
        for r in csv.DictReader(f, delimiter=';'):
            if r['latitude'] != '' and r['longitude'] != '':
                nodes.append(Node(x=float(r['longitude']), y=float(r['latitude']), label=r['organom']))

    dbscan(nodes, 500, 5, region_query)

    colors = ["white", "purple", "blue", "yellow", "green", "light green", "light blue", "light yellow", "light purple", "red",
              "brown", "black", "grey", "orange", "red dust", "spring", "maize", "olive", "tree", "forest", "mold", "teal",
              "sky", "cloud", "lavender", "fuscia", "sunset", "burgundy", "lilac", "sunflower", "candle"] # len = 31

    '''
    clusters = Counter()
    for node in nodes:
        clusters[node.cluster] += 1
    for i in range(len(clusters)):
        print('{0} : {1}'.format(i, clusters[i]))
    '''

    with open("map.csv", "w") as f:
        f.write("Name;Latitude;Longitude;Color\n")
        for n in nodes:
            f.write("{0};{1};{2};{3}\n".format(n.label, n.y, n.x, colors[n.cluster]))